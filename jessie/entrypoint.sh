#!/bin/bash
set -e


update_config_env() {

  export DOLLAR="$"

  export NGINX_LISTEN=${NGINX_LISTEN:=80}
  export NGINX_SERVER_NAME=${NGINX_SERVER_NAME:="_"}
  export NGINX_LOCATION=${NGINX_LOCATION:="/"}
  export NGINX_EXPIRES=${NGINX_EXPIRES:="1h"}
  export NGINX_AUTH_BASIC=${NGINX_AUTH_BASIC:=0}
  export NGINX_AUTH_BASIC_FILE=${NGINX_AUTH_BASIC_FILE:='/etc/nginx/.htpasswd'}
  export NGINX_DNS_RESOLVER=${NGINX_DNS_RESOLVER:="172.16.0.23"}

  if [ $NGINX_AUTH_BASIC = "1" ]
  then
    export NGINX_AUTH_BASIC_content="
      auth_basic 'Restricted Content';
      auth_basic_user_file '${NGINX_AUTH_BASIC_FILE}';
      "
  fi

  if [ $AWS_ACCESS_KEY != "" ]
  then
    export NGINX_S3_AUTH_content="
      aws_access_key          '${AWS_ACCESS_KEY}';
      aws_secret_key          '${AWS_SECRET_KEY}';
      proxy_set_header        Authorization ${DOLLAR}s3_auth_token;
      proxy_set_header        x-amz-date ${DOLLAR}aws_date;
     "
  fi

  envsubst < /default.conf > /etc/nginx/conf.d/default.conf
 # cat /etc/nginx/conf.d/default.conf
}

update_config_env

# allow arguments to be passed to nginx
if [[ ${1:0:1} = '-' ]]; then
  EXTRA_ARGS="$@"
  set --
elif [[ ${1} == nginx || ${1} == $(which nginx) ]]; then
  EXTRA_ARGS="${@:2}"
  set --
fi

# default behaviour is to launch nginx
if [[ -z ${1} ]]; then
  echo "Starting nginx..."
  exec $(which nginx) -c /etc/nginx/nginx.conf ${EXTRA_ARGS}
else
  exec "$@"
fi
